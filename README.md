# H.S. Portfolio
My High School Portfolio

## Live Version
There is a development version of the portfolio hosted [here][dev-version].

There is also a production version of the portfolio hosted [here][prod-version].


[dev-version]:  https://brettbender.gitlab.io/hs-portfolio/
[prod-version]: https://hs.brettbender.me

## Miscellaneous Notes
Before anyone says I'm doing the CSS & JS paths in interesting ways with the 
submodules, that is due to the fact they were designed as an individual project
before they were added to this repository.

## Requirements

### 2018-19 (Freshman)
#### Quarter 1
* [X] Create your homepage
* [X] Customize your homepage

#### Quarter 2
* [ ] First Artifact
* [ ] Schedule

#### Quarter 3
* [X] Complete Career Matchmaker Assesment
* [ ] Create Career Matchmaker page
* [ ] Second Artifact
* [ ] Personal Interest Artifact
